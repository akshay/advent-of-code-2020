let
  sources = import ./nix/sources.nix;
  pkgs = import sources.nixpkgs {};
in
with pkgs; mkShell {
  name = "aoc2020";
  buildInputs = [
    pkgconfig
    zlib
    haskell.compiler.ghc8102
    haskellPackages.cabal-install
    ncurses
  ];
  shellHook = ''
    export LD_LIBRARY_PATH=${ncurses}/lib:${zlib}/lib
    '';
}
