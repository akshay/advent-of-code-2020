module Day06 where

import Data.Bits (Bits (setBit), complement, popCount, zeroBits, (.&.), (.|.))
import qualified Data.Char as Char
import Data.Word (Word32)
import Text.ParserCombinators.ReadP (ReadP)
import qualified Text.ParserCombinators.ReadP as Parser

day6_1 :: IO ()
day6_1 = do
  groupAnswers <- readInput
  let solution = sum $ map orGroupAnswers groupAnswers
  putStrLn $ "Solution: " <> show solution

day6_2 :: IO ()
day6_2 = do
  groupAnswers <- readInput
  let solution = sum $ map andGroupAnswers groupAnswers
  putStrLn $ "Solution: " <> show solution

orGroupAnswers :: GroupAnswer -> Int
orGroupAnswers xs =
  popCount $ foldr (.|.) 0 xs

andGroupAnswers :: GroupAnswer -> Int
andGroupAnswers xs =
  popCount $ foldr (.&.) (complement zeroBits) xs

-- Each bit from 0 to 25 represents each answer
type PersonAnswer = Word32

type GroupAnswer = [PersonAnswer]

readInput :: IO [GroupAnswer]
readInput = do
  str <- getContents
  case Parser.readP_to_S (Parser.sepBy1 readGroupAnswer (newline *> newline) <* Parser.eof) str of
    [(answers, _)] -> pure answers
    _ -> error "invalid input"
  where
    newline = Parser.char '\n'

readPersonAnswer :: ReadP PersonAnswer
readPersonAnswer = do
  trues <- Parser.many1 $ Parser.satisfy Char.isAsciiLower
  pure $ foldr (\c x -> x `setBit` (Char.ord c - Char.ord 'a')) 0 trues

readGroupAnswer :: ReadP GroupAnswer
readGroupAnswer =
  Parser.sepBy1 readPersonAnswer (Parser.char '\n')
