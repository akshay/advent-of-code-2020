module Day15 where

import Data.Foldable (Foldable (foldl'))
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap

day15_1 :: IO ()
day15_1 = do
  input <- readInput
  let (t20thN, _) = foldl' (flip next) (last input, indexInput input) [(length input) .. 2019]
  putStrLn $ "Solution: " <> show t20thN

day15_2 :: IO ()
day15_2 = do
  input <- readInput
  let (t20thN, _) = foldl' (flip next) (last input, indexInput input) [(length input) .. (30000000 - 1)]
  putStrLn $ "Solution: " <> show t20thN

next :: Int -> (Int, HashMap Int Int) -> (Int, HashMap Int Int)
next turn (n, memory) =
  let nextN = case HashMap.lookup n memory of
        Nothing -> 0
        Just prev -> turn - prev
      newMem = HashMap.insert n turn memory
   in (nextN, newMem)

indexInput :: [Int] -> HashMap Int Int
indexInput input = foldr (\(i, n) -> HashMap.insert n i) mempty $ zip [1 ..] (init input)

readInput :: IO [Int]
readInput = do
  str <- getContents
  pure $ read $ "[" <> str <> "]"
