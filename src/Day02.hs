{-# LANGUAGE RecordWildCards #-}

module Day02 where

import Data.Char (isDigit)
import Data.Text (Text)
import qualified Data.Text as Text
import Text.ParserCombinators.ReadP (ReadP)
import qualified Text.ParserCombinators.ReadP as Parser

day2_1 :: IO ()
day2_1 = do
  inputs <- readInput
  let solution = length $ filter (uncurry isValidSledRentalPassword) inputs
  putStrLn $ "Solution: " <> show solution

day2_2 :: IO ()
day2_2 = do
  inputs <- readInput
  let solution = length $ filter (uncurry isValidTobogganRentalPassword) inputs
  putStrLn $ "Solution: " <> show solution

data Policy = Policy
  { policyPos1 :: Int,
    policyPos2 :: Int,
    policyLetter :: Char
  }

type Password = Text

isValidSledRentalPassword :: Policy -> Password -> Bool
isValidSledRentalPassword Policy {..} password =
  let count = Text.count (Text.singleton policyLetter) password
   in count >= policyPos1 && count <= policyPos2

isValidTobogganRentalPassword :: Policy -> Password -> Bool
isValidTobogganRentalPassword Policy {..} password =
  let pos1 = Text.index password (policyPos1 - 1)
      pos2 = Text.index password (policyPos2 - 1)
   in pos1 /= pos2 && (pos1 == policyLetter || pos2 == policyLetter)

readInput :: IO [(Policy, Password)]
readInput =
  map parseLine . lines <$> getContents
  where
    parseLine :: String -> (Policy, Password)
    parseLine s = case Parser.readP_to_S lineParser s of
      [(policy, password)] -> (policy, Text.pack password)
      _ -> error "failed to parse!!"

    lineParser :: ReadP Policy
    lineParser = do
      policyPos1 <- fmap read $ Parser.many1 $ Parser.satisfy isDigit
      _ <- Parser.char '-'
      policyPos2 <- fmap read $ Parser.many1 $ Parser.satisfy isDigit
      Parser.skipSpaces
      policyLetter <- Parser.get
      _ <- Parser.char ':'
      Parser.skipSpaces
      pure $ Policy {..}
