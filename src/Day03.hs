{-# LANGUAGE LambdaCase #-}

module Day03 where

import Data.HashSet (HashSet)
import qualified Data.HashSet as HashSet

day3_1 :: IO ()
day3_1 = do
  (trees, width, depth) <- readInput
  let treesEncountered =
        HashSet.size $
          HashSet.filter
            (isTree trees width)
            (intCoordsOfSlope 3 1 0 depth)
  putStrLn $ "Solution: " <> show treesEncountered

day3_2 :: IO ()
day3_2 = do
  (trees, width, depth) <- readInput
  let treesEncounteredAlong mx my =
        HashSet.size $
          HashSet.filter
            (isTree trees width)
            (intCoordsOfSlope mx my 0 depth)
  putStrLn $
    "Solution: "
      <> show
        ( treesEncounteredAlong 1 1
            * treesEncounteredAlong 3 1
            * treesEncounteredAlong 5 1
            * treesEncounteredAlong 7 1
            * treesEncounteredAlong 1 2
        )

type TreeCoords = HashSet (Int, Int)

intCoordsOfSlope :: Int -> Int -> Int -> Int -> HashSet (Int, Int)
intCoordsOfSlope mx my minY maxY
  | minY > maxY = HashSet.empty
  | (mx * minY) `mod` my == 0 = HashSet.insert ((mx * minY) `div` my, minY) (intCoordsOfSlope mx my (minY + 1) maxY)
  | otherwise = intCoordsOfSlope mx my (minY + 1) maxY

isTree :: TreeCoords -> Int -> (Int, Int) -> Bool
isTree coords width (x, y) =
  HashSet.member (x `mod` width, y) coords

readInput :: IO (TreeCoords, Int, Int)
readInput = do
  rows <- lines <$> getContents
  let treeCoords = HashSet.fromList . concatMap (uncurry (findTrees 0)) . zip [0 ..] $ rows
  pure (treeCoords, length (head rows), length rows)
  where
    findTrees :: Int -> Int -> String -> [(Int, Int)]
    findTrees x y = \case
      "" -> []
      '#' : rest -> (x, y) : findTrees (x + 1) y rest
      _ : rest -> findTrees (x + 1) y rest
