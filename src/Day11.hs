{-# LANGUAGE MultiWayIf #-}

module Day11 where

import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HahsMap
import qualified Data.HashMap.Strict as HashMap
import Data.Maybe (mapMaybe)

day11_1 :: IO ()
day11_1 = do
  initialSeats <- readInput
  putStrLn "--- Initial Seat Map ---"
  printSeatMap initialSeats
  putStrLn ""
  putStrLn "--- Stable Seat Map ---"
  let stableMap = applyRulesTillStability rule1 initialSeats
  printSeatMap stableMap
  putStrLn ""
  putStrLn $ "Solution: " <> show (HashMap.size $ HashMap.filter (== Occupied) stableMap)

day11_2 :: IO ()
day11_2 = do
  initialSeats <- readInput
  putStrLn "--- Initial Seat Map ---"
  printSeatMap initialSeats
  putStrLn ""
  putStrLn "--- Stable Seat Map ---"
  let stableMap = applyRulesTillStability rule2 initialSeats
  printSeatMap stableMap
  putStrLn ""
  putStrLn $ "Solution: " <> show (HashMap.size $ HashMap.filter (== Occupied) stableMap)

data SeatState
  = Floor
  | Empty
  | Occupied
  deriving (Eq, Show)

type Coords = (Int, Int)

type SeatMap = HashMap Coords SeatState

printSeatMap :: SeatMap -> IO ()
printSeatMap m = do
  let (maxX, maxY) = maximum $ HashMap.keys m
  mapM_ (printRow m maxX) [0 .. maxY]

printRow :: SeatMap -> Int -> Int -> IO ()
printRow m maxX y = do
  mapM_ (printPosition m y) [0 .. maxX]
  putStrLn ""

printPosition :: SeatMap -> Int -> Int -> IO ()
printPosition m y x =
  case HashMap.lookup (x, y) m of
    Nothing -> putStr " "
    Just Floor -> putStr "."
    Just Empty -> putStr "L"
    Just Occupied -> putStr "#"

applyRulesTillStability :: (SeatMap -> Coords -> SeatState -> SeatState) -> SeatMap -> SeatMap
applyRulesTillStability f m = do
  let nextMap = applyRule f m
  if nextMap == m
    then m
    else applyRulesTillStability f nextMap

applyRule :: (SeatMap -> Coords -> SeatState -> SeatState) -> SeatMap -> SeatMap
applyRule f seatMap =
  HashMap.mapWithKey (f seatMap) seatMap

rule1 :: SeatMap -> Coords -> SeatState -> SeatState
rule1 m k state =
  let occupiedNeighbours = length $ filter (isOccupied m) (rule1Neighbours k)
   in if
          | state == Occupied && occupiedNeighbours >= 4 -> Empty
          | state == Empty && occupiedNeighbours == 0 -> Occupied
          | otherwise -> state

rule1Neighbours :: Coords -> [Coords]
rule1Neighbours (x, y) =
  let xs = [x + 1, x, x - 1]
      ys = [y + 1, y, y - 1]
   in [(p, q) | p <- xs, q <- ys, not (p == x && q == y)]

rule2 :: SeatMap -> Coords -> SeatState -> SeatState
rule2 m k state =
  let occupiedNeighbours = length $ filter (isOccupied m) (rule2Neighbours m k)
   in if
          | state == Occupied && occupiedNeighbours >= 5 -> Empty
          | state == Empty && occupiedNeighbours == 0 -> Occupied
          | otherwise -> state

rule2Neighbours :: SeatMap -> Coords -> [Coords]
rule2Neighbours m c =
  let right (x, y) = (x + 1, y)
      left (x, y) = (x - 1, y)
      up (x, y) = (x, y - 1)
      down (x, y) = (x, y + 1)
      diag1 (x, y) = (x -1, y -1)
      diag2 (x, y) = (x + 1, y -1)
      diag3 (x, y) = (x + 1, y + 1)
      diag4 (x, y) = (x - 1, y + 1)
   in mapMaybe (\f -> neighbourSeat f m c) [right, left, up, down, diag1, diag2, diag3, diag4]

neighbourSeat :: (Coords -> Coords) -> SeatMap -> Coords -> Maybe Coords
neighbourSeat f m c =
  let next = f c
   in case HashMap.lookup next m of
        Nothing -> Nothing
        Just Floor -> neighbourSeat f m next
        _ -> Just next

isOccupied :: SeatMap -> Coords -> Bool
isOccupied m k = HashMap.lookup k m == Just Occupied

readInput :: IO SeatMap
readInput = do
  parseInput <$> getContents

parseInput :: String -> SeatMap
parseInput s =
  let strs = lines s
   in HahsMap.fromList . concat $
        zipWith parseLine strs [0 ..]
  where
    parseLine :: String -> Int -> [(Coords, SeatState)]
    parseLine str y = zipWith (parseChar y) str [0 ..]
    parseChar y c x
      | c == '.' = ((x, y), Floor)
      | c == 'L' = ((x, y), Empty)
      | c == '#' = ((x, y), Occupied)
      | otherwise = error "Invalid input"
