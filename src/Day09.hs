module Day09 where

import Control.Monad (join)
import Data.Foldable (find)
import Data.List (tails)
import Data.Maybe (isJust)

day9_1 :: IO ()
day9_1 = do
  input <- readInput
  putStrLn $ "Solution: " <> show (findInvalidNumber input)

day9_2 :: IO ()
day9_2 = do
  input <- readInput
  let invalidNumber = findInvalidNumber input
      maybeWeaknessGroup =
        join $
          find isJust $
            map
              ( maybe
                  Nothing
                  ( \xs ->
                      if length xs >= 2
                        then Just xs
                        else Nothing
                  )
                  . headAddsUpTo invalidNumber
              )
              (tails input)
  case maybeWeaknessGroup of
    Nothing -> putStrLn "No two consecutive numbers found"
    Just weaknessGroup ->
      putStrLn $ "Solution: " <> show (minimum weaknessGroup + maximum weaknessGroup)

findInvalidNumber :: [Int] -> Int
findInvalidNumber input =
  let preambleLength = 25
   in case find (not . isValid preambleLength) (tails input) of
        Nothing -> error "No invalid numbers"
        Just x -> x !! preambleLength

isValid :: Int -> [Int] -> Bool
isValid preambleLength input =
  let (preamble, cipher) = splitAt preambleLength input
      target = head cipher
   in not $ null [(x, y) | x <- preamble, y <- preamble, x + y == target]

headAddsUpTo :: Int -> [Int] -> Maybe [Int]
headAddsUpTo weakness input = go weakness input []
  where
    go 0 _ ys = Just ys
    go _ [] _ = Nothing
    go n (x : xs) ys =
      if x > n
        then Nothing
        else go (n - x) xs (x : ys)

readInput :: IO [Int]
readInput =
  map read . lines <$> getContents
