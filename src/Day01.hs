module Day01 where

day1_1 :: IO ()
day1_1 = do
  input <- readInput
  case [(x, y) | x <- input, y <- input, x + y == 2020] of
    [] -> putStrLn "No solutions"
    (x, y) : _ -> putStrLn $ "Found solution: " <> show (x * y)

day1_2 :: IO ()
day1_2 = do
  input <- readInput
  case [(x, y, z) | x <- input, y <- input, z <- input, x + y + z == 2020] of
    [] -> putStrLn "No solutions"
    (x, y, z) : _ -> putStrLn $ "Found solution: " <> show (x * y * z)

readInput :: IO [Int]
readInput =
  map read . lines <$> getContents
