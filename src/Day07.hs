{-# LANGUAGE LambdaCase #-}

module Day07 where

import qualified Data.Char as Char
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Data.List (nub)
import Text.ParserCombinators.ReadP (ReadP, (+++))
import qualified Text.ParserCombinators.ReadP as Parser

day7_1 :: IO ()
day7_1 = do
  contentMap <- readInput
  let containerMap = invert contentMap
      containers = nub $ findAllContainers containerMap "shiny gold"
  putStrLn $ "Solution: " <> show (length containers)

day7_2 :: IO ()
day7_2 = do
  contentMap <- readInput
  putStrLn $ "Solution: " <> show (countContents contentMap "shiny gold")

type Bag = String

type BagContents = [(Int, Bag)]

type BagContainers = [(Int, Bag)]

findAllContainers :: HashMap Bag BagContainers -> Bag -> [Bag]
findAllContainers containerMap b =
  let containers = snd <$> HashMap.lookupDefault [] b containerMap
   in containers <> foldMap (findAllContainers containerMap) containers

countContents :: HashMap Bag BagContents -> Bag -> Int
countContents contentMap b =
  let contentsWithCount = HashMap.lookupDefault [] b contentMap
      (counts, _) = unzip contentsWithCount
   in sum counts + foldr (\(c1, b1) c -> c + (c1 * countContents contentMap b1)) 0 contentsWithCount

invert :: HashMap Bag BagContents -> HashMap Bag BagContainers
invert =
  HashMap.foldrWithKey
    ( \container vs m ->
        foldr (\(n, content) -> HashMap.insertWith (<>) content [(n, container)]) m vs
    )
    mempty

readInput :: IO (HashMap Bag BagContents)
readInput = do
  str <- getContents
  case Parser.readP_to_S (Parser.sepBy1 parseRule (Parser.char '\n') <* Parser.eof) str of
    [(parsed, "")] -> pure (HashMap.fromList parsed)
    _ -> error "Invalid Input"

parseRule :: ReadP (Bag, [(Int, Bag)])
parseRule = do
  outerBag <- parseBag
  _ <- Parser.string " bags contain "
  innerBags <- parseInnerBags
  _ <- Parser.char '.'
  pure (outerBag, innerBags)
  where
    parseWord = Parser.many1 $ Parser.satisfy (not . Char.isSpace)
    parseBag = mconcat <$> sequenceA [parseWord, Parser.string " ", parseWord]
    parseInnerBags =
      parseNoInnerBag
        +++ Parser.sepBy1 parseSomeInnerBag (Parser.string ", ")
    parseNoInnerBag = [] <$ Parser.string "no other bags"
    parseSomeInnerBag = do
      n <- read . (: []) <$> Parser.satisfy Char.isDigit -- Assume 0-9 bags digit
      _ <- Parser.char ' '
      bag <- parseBag
      _ <- Parser.string " bags" +++ Parser.string " bag"
      pure (n, bag)
