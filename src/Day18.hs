module Day18 where

import qualified Data.Char as Char
import qualified Data.List as List
import Text.ParserCombinators.ReadP (ReadP, (+++))
import qualified Text.ParserCombinators.ReadP as Parser

day18_1 :: IO ()
day18_1 = do
  exprs <- readInput1
  putStrLn $ "Solution: " <> show (sum (map evalExpr exprs))

day18_2 :: IO ()
day18_2 = do
  exprs <- readInput2
  putStrLn $ "Solution: " <> show (sum (map evalExpr exprs))

data Expr
  = Val Int
  | Add Expr Expr
  | Mul Expr Expr
  deriving (Eq, Show)

evalExpr :: Expr -> Int
evalExpr (Val x) = x
evalExpr (Add op1 op2) = evalExpr op1 + evalExpr op2
evalExpr (Mul op1 op2) = evalExpr op1 * evalExpr op2

readInput1 :: IO [Expr]
readInput1 = do
  str <- getContents
  case Parser.readP_to_S (Parser.sepBy1 parseExpr1 (Parser.char '\n') <* Parser.eof) str of
    [(exprs, "")] -> pure exprs
    _ -> error "Invalid Input"

parseExpr1 :: ReadP Expr
parseExpr1 = go =<< parseOperand
  where
    go :: Expr -> ReadP Expr
    go op1 = do
      rest <- Parser.look
      if rest == "" || head rest == '\n' || head rest == ')'
        then pure op1
        else do
          newOp1 <- parseRestOfTheOperation op1
          go newOp1
    parseRestOfTheOperation :: Expr -> ReadP Expr
    parseRestOfTheOperation op1 = parseRestOfAdd op1 +++ parseRestOfMul op1

    parseRestOfAdd :: Expr -> ReadP Expr
    parseRestOfAdd op1 = do
      _ <- Parser.string " + "
      Add op1 <$> parseOperand

    parseRestOfMul :: Expr -> ReadP Expr
    parseRestOfMul op1 = do
      _ <- Parser.string " * "
      Mul op1 <$> parseOperand

    parseOperand :: ReadP Expr
    parseOperand = parseVal +++ parseParens

    parseParens :: ReadP Expr
    parseParens = Parser.char '(' *> parseExpr1 <* Parser.char ')'

    parseVal :: ReadP Expr
    parseVal = fmap Val parseInt

type UnprecedentedExpr = [Token]

data Token
  = TokenVal Int
  | TokenPlus
  | TokenMul
  | TokenSubExpr UnprecedentedExpr
  deriving (Show, Eq)

readInput2 :: IO [Expr]
readInput2 = do
  str <- getContents
  case Parser.readP_to_S (Parser.sepBy1 parseUnprecedentedExpr (Parser.char '\n') <* Parser.eof) str of
    [(exprs, "")] -> pure $ map assignPrecedece exprs
    _ -> error "Invalid Input"

assignPrecedece :: UnprecedentedExpr -> Expr
assignPrecedece tokens =
  case List.elemIndex TokenMul tokens of
    Nothing ->
      case List.elemIndex TokenPlus tokens of
        Nothing -> case tokens of
          [] -> error "Invalid expr"
          [TokenVal x] -> Val x
          [TokenSubExpr expr] -> assignPrecedece expr
          _ -> error "Invalid expr"
        Just i ->
          let (expr1, _ : expr2) = List.splitAt i tokens
           in Add (assignPrecedece expr1) (assignPrecedece expr2)
    Just i ->
      let (expr1, _ : expr2) = List.splitAt i tokens
       in Mul (assignPrecedece expr1) (assignPrecedece expr2)

parseUnprecedentedExpr :: ReadP UnprecedentedExpr
parseUnprecedentedExpr = Parser.many1 parseToken
  where
    parseToken :: ReadP Token
    parseToken =
      parseIntToken
        +++ parseSubExpr
        +++ parsePlus
        +++ parseMul

    parseSubExpr :: ReadP Token
    parseSubExpr = Parser.char '(' *> fmap TokenSubExpr parseUnprecedentedExpr <* Parser.char ')'

    parseIntToken :: ReadP Token
    parseIntToken = fmap TokenVal parseInt

    parsePlus :: ReadP Token
    parsePlus = Parser.string " + " >> pure TokenPlus

    parseMul :: ReadP Token
    parseMul = Parser.string " * " >> pure TokenMul

parseInt :: ReadP Int
parseInt = read <$> Parser.many1 (Parser.satisfy Char.isDigit)
