module Day16 where

import qualified Data.Char as Char
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import qualified Data.List as List
import Data.Maybe (listToMaybe)
import Text.ParserCombinators.ReadP (ReadP)
import qualified Text.ParserCombinators.ReadP as Parser

day16_1 :: IO ()
day16_1 = do
  (fieldRuleMapping, _ownTicket, nearbyTickets) <- readInput
  let intervals = concatMap (ruleToIntervals . snd) fieldRuleMapping
      invalidValues = filter (not . matchAnyInterval intervals) $ concat nearbyTickets
      errorRate = sum invalidValues
  putStrLn $ "Solution: " <> show errorRate

day16_2 :: IO ()
day16_2 = do
  (fieldRuleMapping, ownTicket, nearbyTickets) <- readInput
  let ruleBook = HashMap.fromList fieldRuleMapping
      validNearbyTickets = filter (isTicketValid $ map snd fieldRuleMapping) nearbyTickets
      allPossibilities = replicate (length ownTicket) (map fst fieldRuleMapping)
      finalPossibilities = foldr (filterPossibilities ruleBook) allPossibilities validNearbyTickets
      collapsedPossiblities = collapsePossibilities mempty (HashMap.fromList $ zip [0 ..] finalPossibilities)
      departureIndices = HashMap.keys $ HashMap.filter (\name -> "departure" `List.isPrefixOf` name) collapsedPossiblities
      departureDetails = map (ownTicket !!) departureIndices
  putStrLn $ "Solution: " <> show (product departureDetails)

type Rule = ((Int, Int), (Int, Int))

type TicketNumbers = [Int]

matchAnyInterval :: [(Int, Int)] -> Int -> Bool
matchAnyInterval [] _ = False
matchAnyInterval ((l, h) : is) n = (n >= l && n <= h) || matchAnyInterval is n

ruleToIntervals :: (a, a) -> [a]
ruleToIntervals (r1, r2) = [r1, r2]

matchRule :: Rule -> Int -> Bool
matchRule (i1, i2) = matchAnyInterval [i1, i2]

filterPossibilities :: HashMap String Rule -> TicketNumbers -> [[String]] -> [[String]]
filterPossibilities _ruleBook [] _ = []
filterPossibilities _ruleBook _ [] = []
filterPossibilities ruleBook (n : ns) (ps : pss) =
  let remainaingPossibilities = filter (\p -> matchRule (HashMap.lookupDefault undefined p ruleBook) n) ps
   in remainaingPossibilities : filterPossibilities ruleBook ns pss

isTicketValid :: [Rule] -> TicketNumbers -> Bool
isTicketValid rules = all (\n -> any (`matchRule` n) rules)

collapsePossibilities :: HashMap Int String -> HashMap Int [String] -> HashMap Int String
collapsePossibilities alreadyCollapsed possibilitiesMap =
  case listToMaybe $ HashMap.keys $ HashMap.filter (\ps -> length ps == 1) possibilitiesMap of
    Nothing -> alreadyCollapsed
    Just i ->
      let onlyPossibility = head $ HashMap.lookupDefault undefined i possibilitiesMap
          newCollapsed = HashMap.insert i onlyPossibility alreadyCollapsed
          newPossibilities = HashMap.map (List.delete onlyPossibility) $ HashMap.delete i possibilitiesMap
       in collapsePossibilities newCollapsed newPossibilities

readInput ::
  IO
    ( [(String, Rule)], -- [(field, rule)]
      TicketNumbers, -- own ticket
      [TicketNumbers] -- other tickets
    )
readInput = do
  str <- getContents
  case Parser.readP_to_S (parseInput <* Parser.eof) str of
    [(input, "")] -> pure input
    _ -> error "Invalid input"

parseRule :: ReadP (String, Rule)
parseRule = do
  field <- Parser.many1 $ Parser.satisfy (/= ':')
  _ <- Parser.string ": "
  interval1 <- parseInterval
  _ <- Parser.string " or "
  interval2 <- parseInterval
  pure (field, (interval1, interval2))

parseInterval :: ReadP (Int, Int)
parseInterval = do
  lower <- parseInt
  _ <- Parser.char '-'
  higher <- parseInt
  pure (lower, higher)

parseTicketNumbers :: ReadP TicketNumbers
parseTicketNumbers =
  Parser.sepBy1 parseInt (Parser.char ',')

parseInt :: ReadP Int
parseInt = fmap read . Parser.many1 $ Parser.satisfy Char.isDigit

parseInput ::
  ReadP
    ( [(String, Rule)], -- [(field, rule)]
      TicketNumbers, -- own ticket
      [TicketNumbers] -- nearby tickets
    )
parseInput = do
  rules <- Parser.sepBy1 parseRule (Parser.char '\n')
  _ <- Parser.string "\n\nyour ticket:\n"
  ownTicket <- parseTicketNumbers
  _ <- Parser.string "\n\nnearby tickets:\n"
  nearbyTickets <- Parser.sepBy1 parseTicketNumbers (Parser.char '\n')
  pure (rules, ownTicket, nearbyTickets)
