module Day12 where

import Data.Foldable (Foldable (foldl'))

day12_1 :: IO ()
day12_1 = do
  instructions <- readInput
  let (_finalFacing, (x, y)) = foldl' executeInstruction1 (FE, (0, 0)) instructions
  putStrLn $ "Solution: " <> show (abs x + abs y)

day12_2 :: IO ()
day12_2 = do
  instructions <- readInput
  let (_finalFacing, (x, y)) = foldl' executeInstruction2 ((10, -1), (0, 0)) instructions
  putStrLn $ "Solution: " <> show (abs x + abs y)

data InstructionType = F | N | E | W | S | L | R
  deriving (Show, Read)

type Instruction = (InstructionType, Int)

data Facing = FN | FE | FW | FS
  deriving (Show, Eq)

type Coords = (Int, Int)

executeInstruction1 :: (Facing, Coords) -> Instruction -> (Facing, Coords)
executeInstruction1 shipState@(facing, coords) (instrType, magnitude) =
  case instrType of
    F -> moveShipForward magnitude shipState
    N -> (facing, moveNorthCoords magnitude coords)
    E -> (facing, moveEastCoords magnitude coords)
    W -> (facing, moveWestCoords magnitude coords)
    S -> (facing, moveSouthCoords magnitude coords)
    L -> (rotateShipLeft magnitude facing, coords)
    R -> (rotateShipRight magnitude facing, coords)

executeInstruction2 :: (Coords, Coords) -> Instruction -> (Coords, Coords)
executeInstruction2 (waypoint, ship) (instrType, magnitude) =
  case instrType of
    F -> (waypoint, moveShipToWaypoint magnitude waypoint ship)
    N -> (moveNorthCoords magnitude waypoint, ship)
    E -> (moveEastCoords magnitude waypoint, ship)
    W -> (moveWestCoords magnitude waypoint, ship)
    S -> (moveSouthCoords magnitude waypoint, ship)
    L -> (rotateLeftCoords magnitude waypoint, ship)
    R -> (rotateRightCoords magnitude waypoint, ship)

moveShipForward :: Int -> (Facing, Coords) -> (Facing, Coords)
moveShipForward n (facing, coords) =
  let newCoords = case facing of
        FN -> moveNorthCoords n coords
        FE -> moveEastCoords n coords
        FW -> moveWestCoords n coords
        FS -> moveSouthCoords n coords
   in (facing, newCoords)

moveShipToWaypoint :: Int -> Coords -> Coords -> Coords
moveShipToWaypoint n (wx, wy) (sx, sy) =
  (sx + n * wx, sy + n * wy)

moveNorthCoords, moveEastCoords, moveWestCoords, moveSouthCoords :: Int -> Coords -> Coords
moveNorthCoords n (x, y) = (x, y - n)
moveEastCoords n (x, y) = (x + n, y)
moveWestCoords n (x, y) = (x - n, y)
moveSouthCoords n (x, y) = (x, y + n)

rotateShipLeft, rotateShipRight :: Int -> Facing -> Facing
rotateShipLeft = rotateShipRight . (360 -)
rotateShipRight deg facing =
  let rotations = cycle [FN, FE, FS, FW]
   in dropWhile (/= facing) rotations !! (deg `div` 90)

-- Maybe there is a formula for rotating a point, where y axis is inverted, but
-- this works!
rotateLeftCoords, rotateRightCoords :: Int -> Coords -> Coords
rotateLeftCoords = rotateRightCoords . (360 -)
rotateRightCoords deg (east, south) =
  case deg of
    90 -> (- south, east) -- North becomes east, east becomes south
    180 -> (- east, - south) -- North becomes south, east becomes west
    270 -> (south, - east) -- North becomes west, east becomes north
    360 -> (east, south)
    _ -> error "Invalid degrees"

readInput :: IO [Instruction]
readInput = do
  map parseLine . lines <$> getContents
  where
    parseLine (x : xs) = (read [x], read xs)
    parseLine _ = error "Invalid input"
