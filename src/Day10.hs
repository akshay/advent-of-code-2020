module Day10 where

import Data.List (sort)
import Data.List.Extra (split)

day10_1 :: IO ()
day10_1 = do
  input <- sort <$> readInput
  let diffs = differences input
      oneJoltDiffs = length $ filter (== 1) diffs
      threeJoltDiffs = length $ filter (== 3) diffs
  putStrLn $ "Solution: " <> show (oneJoltDiffs * threeJoltDiffs)

day10_2 :: IO ()
day10_2 = do
  input <- sort <$> readInput
  let diffs = differences input
      consecutiveOneJolts = filter (\xs -> length xs >= 2) $ split (== 3) diffs
      combos = map (combinations . length) consecutiveOneJolts
  putStrLn $ "Solution: " <> show (product combos)

differences :: [Int] -> [Int]
differences input = zipWith (-) (input ++ [maximum input + 3]) (0 : input)

combinations :: Int -> Int
combinations 1 = 1 -- There is only one 1, it can only be used as is
combinations 2 = 2 -- Two 1s can be used as: [1,1] or [2]
combinations 3 = 4 -- Three 1s can be used as: [1,1,1], [1,2], [2,1] or [3]
combinations n =
  combinations (n - 1) -- Use the first 1 as is, , there are (n-1) ones remaining
    + combinations (n - 2) -- Squash first two 1s into a 2, there are (n-2) ones remaining
    + combinations (n - 3) -- Squash first three 1s into a 3, there are (n-3) ones remaining

readInput :: IO [Int]
readInput =
  map read . lines <$> getContents
