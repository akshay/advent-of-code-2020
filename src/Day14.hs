module Day14 where

import Data.Bits (Bits (clearBit), setBit)
import qualified Data.Char as Char
import Data.Foldable (Foldable (foldl'))
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Text.ParserCombinators.ReadP (ReadP, (+++))
import qualified Text.ParserCombinators.ReadP as Parser

day14_1 :: IO ()
day14_1 = do
  instructions <- readInput
  let finalState = foldl' executeInstruction1 (State ([], [], []) mempty) instructions
  putStrLn $ "Solution: " <> show (sum (mem finalState))

day14_2 :: IO ()
day14_2 = do
  instructions <- readInput
  let finalState = foldl' executeInstruction2 (State ([], [], []) mempty) instructions
  putStrLn $ "Solution: " <> show (sum (mem finalState))

data Instruction
  = Mask [Int] [Int] [Int]
  | Mem Int Int

data State = State
  { mask :: ([Int], [Int], [Int]),
    mem :: HashMap Int Int
  }
  deriving (Show)

executeInstruction1 :: State -> Instruction -> State
executeInstruction1 state instr =
  case instr of
    Mask zeros ones xs -> state {mask = (zeros, ones, xs)}
    Mem addr val ->
      state
        { mem =
            HashMap.insert addr (maskVal (mask state) val) (mem state)
        }

executeInstruction2 :: State -> Instruction -> State
executeInstruction2 state instr =
  case instr of
    Mask zeros ones xs -> state {mask = (zeros, ones, xs)}
    Mem addr val ->
      state
        { mem =
            foldr (`HashMap.insert` val) (mem state) $ maskAddr (mask state) addr
        }

maskVal :: ([Int], [Int], [Int]) -> Int -> Int
maskVal (zeros, ones, _) val =
  let afterZeros = foldr (flip clearBit) val zeros
      afterOnes = foldr (flip setBit) afterZeros ones
   in afterOnes

maskAddr :: ([Int], [Int], [Int]) -> Int -> [Int]
maskAddr (_, ones, xs) addr =
  let afterOnes = foldr (flip setBit) addr ones
   in foldr
        ( \n addrs ->
            let withOne = map (`setBit` n) addrs
                withZero = map (`clearBit` n) addrs
             in withOne <> withZero
        )
        [afterOnes]
        xs

readInput :: IO [Instruction]
readInput = do
  str <- getContents
  case Parser.readP_to_S (Parser.sepBy1 parseInstruction (Parser.char '\n') <* Parser.eof) str of
    [(instr, "")] -> pure instr
    _ -> error "Invalid Input"

parseInstruction :: ReadP Instruction
parseInstruction =
  parseMask +++ parseMem
  where
    parseMask = do
      _ <- Parser.string "mask = "
      maskStr <- Parser.munch (\x -> Char.isDigit x || x == 'X')
      let (zeros, ones, xs) =
            foldr
              ( \(n, c) (z, o, x) -> case c of
                  '0' -> (n : z, o, x)
                  '1' -> (z, n : o, x)
                  'X' -> (z, o, n : x)
                  _ -> (z, o, x)
              )
              ([], [], [])
              (zip [0 ..] (reverse maskStr))
      pure $ Mask zeros ones xs
    parseMem = do
      _ <- Parser.string "mem["
      addr <- read <$> Parser.many1 (Parser.satisfy Char.isDigit)
      _ <- Parser.string "] = "
      val <- read <$> Parser.many1 (Parser.satisfy Char.isDigit)
      pure $ Mem addr val
