{-# LANGUAGE TupleSections #-}

module Day13 where

import Data.List.Extra (foldl', minimumOn, splitOn)
import Data.Maybe (catMaybes, mapMaybe)
import Text.Read (readMaybe)

day13_1 :: IO ()
day13_1 = do
  (time, buses) <- readInput1
  let waitTimesByBus = map (\x -> (x, x - time `mod` x)) buses
  putStrLn $ "Solution: " <> (show . uncurry (*) $ minimumOn snd waitTimesByBus)

-- | How this works:
--
-- If two buses start at t = 0 and start every n minutes and m minutes
-- respectively. The next time they start together will be at t = 'lcm n m'.
--
-- So, if there was a time 't' when bus-n starts and bus-m started at t+1, this
-- will be repeated every 'lcm n m' minutes and may not happen again before 't +
-- lcm n m'.
--
-- Now, if we want to add a third bus which leaves every o minutes and should
-- leave at (t + 3), we must compute this by trying values of t in steps of 'lcm
-- n m'. Once we find such 't', we can add a fourth bus by going in steps of
-- 'lcm n m o' and so on. The 'lcm' value will keep increasing and we will be
-- able to skip a lot of computations.
day13_2 :: IO ()
day13_2 = do
  timeMap <- readInput2
  let (solution, _) =
        foldl'
          ( \(start, step) (t, b) ->
              let nextStart = findNextStart start step (t, b)
                  nextStep = lcm b step
               in (nextStart, nextStep)
          )
          (0, 1)
          timeMap
  putStrLn $ "Solution: " <> show solution

type Time = Int

type Bus = Int

findNextStart :: Int -> Int -> (Int, Bus) -> Int
findNextStart start step (t, b) = go start
  where
    go n =
      if (n + t) `mod` b == 0
        then n
        else go (n + step)

readInput1 :: IO (Time, [Bus])
readInput1 = do
  time <- read <$> getLine
  buses <- mapMaybe readMaybe . splitOn "," <$> getLine
  pure (time, buses)

readInput2 :: IO [(Int, Bus)]
readInput2 = do
  _ <- getLine
  parseInput2 <$> getLine

parseInput2 :: String -> [(Int, Bus)]
parseInput2 = catMaybes . zipWith (\i b -> (i,) <$> readMaybe b) [0 ..] . splitOn ","
