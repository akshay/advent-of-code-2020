{-# LANGUAGE RecordWildCards #-}

module Day04 where

import qualified Data.Char as Char
import qualified Data.HashMap.Strict as HashMap
import Data.HashSet (HashSet)
import qualified Data.HashSet as HashSet
import Data.Maybe (catMaybes)
import Text.ParserCombinators.ReadP (ReadP, (+++))
import qualified Text.ParserCombinators.ReadP as Parser
import Text.Read (readMaybe)

day4_1 :: IO ()
day4_1 = do
  passports <- readInput1
  let validPassports = filter (HashSet.null . HashSet.difference mandatoryKeys) passports
  putStrLn $ "Solution: " <> show (length validPassports)

day4_2 :: IO ()
day4_2 = do
  validPassports <- readInput2
  putStrLn $ "Solution: " <> show (length validPassports)

type Key = String

type PassportKeys = HashSet Key

-- TODO: Maybe represent ranged integers specially
-- TODO: Maybe represent hex digits specially
-- TODO: Maybe represent digits specially
data Passport = Passport
  { byr :: Int,
    iyr :: Int,
    eyr :: Int,
    hgt :: Height,
    hcl :: (Char, Char, Char, Char, Char, Char),
    ecl :: EyeColor,
    pid :: (Char, Char, Char, Char, Char, Char, Char, Char, Char)
  }

data EyeColor = Amb | Blu | Brn | Gry | Grn | Hzl | Oth

data Height = Centimeters Int | Inches Int

mandatoryKeys :: HashSet Key
mandatoryKeys = HashSet.fromList ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

readPassportKeyVal :: ReadP (String, String)
readPassportKeyVal = do
  key <- Parser.many1 $ Parser.satisfy (\x -> x /= ':' && not (Char.isSpace x))
  _ <- Parser.char ':'
  val <- Parser.many1 $ Parser.satisfy (not . Char.isSpace)
  pure (key, val)

readPassportKey :: ReadP Key
readPassportKey = fst <$> readPassportKeyVal

readPassportKeys :: ReadP PassportKeys
readPassportKeys =
  HashSet.fromList <$> Parser.sepBy1 readPassportKey (Parser.satisfy Char.isSpace)

readInput1 :: IO [PassportKeys]
readInput1 = do
  inputStr <- getContents
  maybe (error "invalid input") pure $ parseOnly (Parser.sepBy1 readPassportKeys (newline *> newline)) inputStr
  where
    newline = Parser.char '\n'

readPassportKeyVals :: ReadP (Maybe Passport)
readPassportKeyVals = do
  lst <- Parser.sepBy1 readPassportKeyVal (Parser.satisfy Char.isSpace)
  let kvs = HashMap.fromList lst
      lookupAndParse parser key = parser =<< HashMap.lookup key kvs
      passport = do
        byr <- lookupAndParse (readRangedInt 1920 2002) "byr"
        iyr <- lookupAndParse (readRangedInt 2010 2020) "iyr"
        eyr <- lookupAndParse (readRangedInt 2020 2030) "eyr"
        hgt <- lookupAndParse readHeight "hgt"
        hcl <- lookupAndParse readHairColor "hcl"
        ecl <- lookupAndParse readEyeColor "ecl"
        pid <- lookupAndParse readPid "pid"
        pure $ Passport {..}
  pure passport

readRangedInt :: Int -> Int -> String -> Maybe Int
readRangedInt lowerBound upperBound str = do
  val <- readMaybe str
  if val >= lowerBound && val <= upperBound
    then Just val
    else Nothing

readHeight :: String -> Maybe Height
readHeight = parseOnly (readCm +++ readIn)
  where
    readCm = do
      -- NOTE: This is unsafe
      num <- read <$> Parser.many1 (Parser.satisfy Char.isDigit)
      _cm <- Parser.string "cm"
      if num >= 150 && num <= 193
        then pure $ Centimeters num
        else Parser.pfail
    readIn = do
      -- NOTE: This is unsafe
      num <- read <$> Parser.many1 (Parser.satisfy Char.isDigit)
      _in <- Parser.string "in"
      if num >= 59 && num <= 76
        then pure $ Inches num
        else Parser.pfail

readHairColor :: String -> Maybe (Char, Char, Char, Char, Char, Char)
readHairColor = parseOnly readColor
  where
    readColor :: ReadP (Char, Char, Char, Char, Char, Char)
    readColor = do
      _ <- Parser.char '#'
      (,,,,,)
        <$> readHex
        <*> readHex
        <*> readHex
        <*> readHex
        <*> readHex
        <*> readHex
    readHex :: ReadP Char
    readHex = Parser.satisfy Char.isHexDigit

readEyeColor :: String -> Maybe EyeColor
readEyeColor = parseOnly readColor
  where
    readColor :: ReadP EyeColor
    readColor =
      (Amb <$ Parser.string "amb")
        +++ (Blu <$ Parser.string "blu")
        +++ (Brn <$ Parser.string "brn")
        +++ (Gry <$ Parser.string "gry")
        +++ (Grn <$ Parser.string "grn")
        +++ (Hzl <$ Parser.string "hzl")
        +++ (Oth <$ Parser.string "oth")

readPid :: String -> Maybe (Char, Char, Char, Char, Char, Char, Char, Char, Char)
readPid = parseOnly parser
  where
    parser :: ReadP (Char, Char, Char, Char, Char, Char, Char, Char, Char)
    parser =
      (,,,,,,,,)
        <$> Parser.satisfy Char.isDigit
        <*> Parser.satisfy Char.isDigit
        <*> Parser.satisfy Char.isDigit
        <*> Parser.satisfy Char.isDigit
        <*> Parser.satisfy Char.isDigit
        <*> Parser.satisfy Char.isDigit
        <*> Parser.satisfy Char.isDigit
        <*> Parser.satisfy Char.isDigit
        <*> Parser.satisfy Char.isDigit

parseOnly :: ReadP a -> String -> Maybe a
parseOnly p s =
  case Parser.readP_to_S (p <* Parser.eof) s of
    [(c, "")] -> Just c
    _ -> Nothing

-- Ignores invalid passports
readPassports :: ReadP [Passport]
readPassports =
  catMaybes <$> Parser.sepBy1 readPassportKeyVals (parseNewLine *> parseNewLine)
  where
    parseNewLine = Parser.satisfy (== '\n')

readInput2 :: IO [Passport]
readInput2 = do
  inputStr <- getContents
  maybe (error "Invalid input") pure $ parseOnly readPassports inputStr
