module Day19 where

import Control.Monad (void)
import qualified Data.Char as Char
import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Text.ParserCombinators.ReadP (ReadP, (+++))
import qualified Text.ParserCombinators.ReadP as Parser

day19_1 :: IO ()
day19_1 = do
  (rules, msgs) <- readInput
  let rule0 = (HashMap.!) rules 0
      matches = filter (\m -> matchesRule rules rule0 m == Just "") msgs
  putStrLn $ "Solution: " <> show (length matches)

day19_2 :: IO ()
day19_2 = do
  (rules, msgs) <- readInput
  let rule0 = (HashMap.!) rules 0
      updatedRules =
        HashMap.insert 8 (RuleChoice (RuleSequence [42]) (RuleSequence [42, 8])) $
          HashMap.insert 11 (RuleChoice (RuleSequence [42, 31]) (RuleSequence [42, 11, 31])) rules
      readP = ruleToReadP updatedRules rule0
      matches = filter (\m -> Parser.readP_to_S (readP <* Parser.eof) m == [((), "")]) msgs
  putStrLn $ "Solution: " <> show (length matches)

data Rule
  = RuleChar Char
  | RuleChoice Rule Rule
  | RuleSequence [Int]
  deriving (Show, Eq)

type Rules = HashMap Int Rule

-- |
-- Nothing means no match
-- Just "" means perfect match
-- Just str means matches with str remaining
matchesRule :: Rules -> Rule -> String -> Maybe String
matchesRule _ (RuleChar _) "" = Nothing
matchesRule _ (RuleChar c) (c' : rest) = if c == c' then Just rest else Nothing
matchesRule rules (RuleChoice r1 r2) str =
  case matchesRule rules r1 str of
    Just x -> Just x
    _ -> matchesRule rules r2 str
matchesRule _ (RuleSequence []) str = Just str
matchesRule rules (RuleSequence (r1id : rest)) str =
  let r1 = (HashMap.!) rules r1id
   in case matchesRule rules r1 str of
        Just remaining -> matchesRule rules (RuleSequence rest) remaining
        Nothing -> Nothing

-- | Is this cheating?
ruleToReadP :: Rules -> Rule -> ReadP ()
ruleToReadP _rules (RuleChar c) = void $ Parser.char c
ruleToReadP rules (RuleChoice r1 r2) = ruleToReadP rules r1 +++ ruleToReadP rules r2
ruleToReadP _rules (RuleSequence []) = pure ()
ruleToReadP rules (RuleSequence (r : rs)) =
  let rule = (HashMap.!) rules r
   in ruleToReadP rules rule >> ruleToReadP rules (RuleSequence rs)

readInput :: IO (Rules, [String])
readInput = do
  str <- getContents
  case Parser.readP_to_S parseInput str of
    [(input, "")] -> pure input
    _ -> error "invalid input"

parseInput :: ReadP (Rules, [String])
parseInput = do
  rules <- HashMap.fromList <$> Parser.sepBy1 parseRule (Parser.char '\n')
  _ <- Parser.string "\n\n"
  strs <- Parser.sepBy1 (Parser.munch1 (/= '\n')) (Parser.char '\n')
  _ <- Parser.skipSpaces
  _ <- Parser.eof
  pure (rules, strs)

parseRule :: ReadP (Int, Rule)
parseRule = do
  ruleId <- parseInt
  _ <- Parser.string ": "
  rule <- parseCharRule +++ parseRuleSequence +++ parseChoices
  pure (ruleId, rule)

parseInt :: ReadP Int
parseInt = read <$> Parser.many1 (Parser.satisfy Char.isDigit)

parseCharRule :: ReadP Rule
parseCharRule =
  fmap RuleChar $
    Parser.char '"'
      *> Parser.satisfy (\x -> x == 'a' || x == 'b')
      <* Parser.char '"'

parseRuleSequence :: ReadP Rule
parseRuleSequence = RuleSequence <$> Parser.sepBy1 parseInt (Parser.char ' ')

parseChoices :: ReadP Rule
parseChoices =
  RuleChoice
    <$> (parseRuleSequence <* Parser.string " | ")
    <*> parseRuleSequence
