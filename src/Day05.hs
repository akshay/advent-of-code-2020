{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}

module Day05 where

import Control.Monad (zipWithM)
import Data.Bits (Bits (bit), (.|.))
import Data.HashSet (HashSet)
import qualified Data.HashSet as HashSet
import Data.Hashable (Hashable)
import Data.Word (Word8)
import GHC.Generics (Generic)
import Text.ParserCombinators.ReadP (ReadP, (+++))
import qualified Text.ParserCombinators.ReadP as Parser
import Text.Printf (printf)

day5_1 :: IO ()
day5_1 = do
  seats <- readInput
  putStrLn $ "Solution: " <> show (seatId (maximum seats))

day5_2 :: IO ()
day5_2 = do
  seats <- HashSet.fromList <$> readInput
  putStrLn "Seat Plan"
  printSeats seats
  let emptySeats = HashSet.difference (HashSet.fromList [minBound .. maxBound]) seats
  case HashSet.toList (HashSet.filter (isBetweenTakenSeats seats) emptySeats) of
    [theSeat] -> putStrLn $ "Solution: " <> show (seatId theSeat)
    [] -> putStrLn "No seats available"
    xs -> putStrLn $ "Many seats available: " <> show xs

isBetweenTakenSeats :: HashSet Seat -> Seat -> Bool
isBetweenTakenSeats seats s =
  HashSet.member (pred s) seats
    && HashSet.member (succ s) seats

printSeats :: HashSet Seat -> IO ()
printSeats seats = do
  let minRow = 0
      maxRow = 128
  mapM_ (printRow seats) [minRow .. maxRow]

printRow :: HashSet Seat -> Word8 -> IO ()
printRow seats r = do
  let minColumn = 0
      maxColumn = 7
      rowNum = printf "%3d " r
  putStr rowNum
  mapM_ (printSeat seats r) [minColumn .. maxColumn]
  putStrLn ""

printSeat :: HashSet Seat -> Word8 -> Word8 -> IO ()
printSeat seats r c =
  if HashSet.member (Seat r c) seats
    then putStr "X"
    else putStr " "

data Seat = Seat
  { row :: Word8,
    column :: Word8
  }
  deriving (Show, Eq, Generic)

instance Ord Seat where
  compare s1 s2 = compare (seatId s1) (seatId s2)

instance Hashable Seat

instance Enum Seat where
  fromEnum = seatId
  toEnum n =
    let column = fromIntegral $ n `mod` 8
        row = fromIntegral $ n `div` 8
     in Seat {..}

instance Bounded Seat where
  minBound = Seat 0 0
  maxBound = Seat 128 7

seatId :: Seat -> Int
seatId Seat {..} = fromIntegral row * 8 + fromIntegral column

readRowBit :: ReadP Bool
readRowBit = (False <$ Parser.char 'F') +++ (True <$ Parser.char 'B')

readColumnBit :: ReadP Bool
readColumnBit = (False <$ Parser.char 'L') +++ (True <$ Parser.char 'R')

readNthBit :: Int -> ReadP Bool -> ReadP Word8
readNthBit n mb = do
  b <- mb
  if b
    then pure (bit n)
    else pure 0

readRow :: ReadP Word8
readRow = do
  bits <- zipWithM readNthBit [6, 5 .. 0] (repeat readRowBit)
  pure $ foldr (.|.) 0 bits

readColumn :: ReadP Word8
readColumn = do
  bits <- zipWithM readNthBit [2, 1, 0] (repeat readColumnBit)
  pure $ foldr (.|.) 0 bits

readInput :: IO [Seat]
readInput = do
  str <- getContents
  case Parser.readP_to_S (Parser.sepBy1 (Seat <$> readRow <*> readColumn) (Parser.char '\n') <* Parser.eof) str of
    [(seats, "")] -> pure seats
    x -> error $ "Invalid input\n" <> show x
