{-# LANGUAGE LambdaCase #-}

module Day08 where

import qualified Data.Char as Char
import Data.HashSet (HashSet)
import qualified Data.HashSet as HashSet
import Lens.Micro (ix, (&), (.~))
import Text.ParserCombinators.ReadP (ReadP, (+++))
import qualified Text.ParserCombinators.ReadP as Parser
import Text.Read (readMaybe)

day8_1 :: IO ()
day8_1 = do
  instructions <- readInput
  case executeAndDetectInfiniteLoop instructions of
    InfiniteLoopDetected acc -> putStrLn $ "Solution: " <> show acc
    Terminated acc -> putStrLn $ "No inifinite loop detectd, program terminated with " <> show acc

day8_2 :: IO ()
day8_2 = do
  instructions <- readInput
  putStrLn $ "Solution: " <> show (findFixedProgram instructions)

data Instruction
  = Acc Int
  | Jmp Int
  | Nop Int

type Acc = Int

type InstructionId = Int

data Result
  = InfiniteLoopDetected Acc
  | Terminated Acc
  deriving (Show)

executeAndDetectInfiniteLoop :: [Instruction] -> Result
executeAndDetectInfiniteLoop instructions =
  doesItTerminate instructions (0, 0) (HashSet.fromList [])

executeInstruction :: (Acc, InstructionId) -> Instruction -> (Acc, InstructionId)
executeInstruction (acc, instructionId) =
  \case
    Acc n -> (acc + n, instructionId + 1)
    Jmp n -> (acc, instructionId + n)
    Nop _ -> (acc, instructionId + 1)

doesItTerminate :: [Instruction] -> (Acc, InstructionId) -> HashSet InstructionId -> Result
doesItTerminate instructions = go
  where
    go (acc, instructionId) executed
      | instructionId `HashSet.member` executed = InfiniteLoopDetected acc
      | instructionId >= length instructions = Terminated acc
      | otherwise =
        let currentInstruction = (instructions !! instructionId)
            newState = executeInstruction (acc, instructionId) currentInstruction
            newExecuted = HashSet.insert instructionId executed
         in go newState newExecuted

findFixedProgram :: [Instruction] -> Acc
findFixedProgram instructions = go (0, 0) (HashSet.fromList [])
  where
    go (acc, instructionId) executed
      | instructionId >= length instructions = error "impossible"
      | otherwise =
        let currentInstruction = instructions !! instructionId
            tryNext =
              let newState = executeInstruction (acc, instructionId) currentInstruction
                  newExecuted = HashSet.insert instructionId executed
               in go newState newExecuted
            tryThis i =
              let newInstructions = instructions & ix instructionId .~ i
               in case doesItTerminate newInstructions (acc, instructionId) executed of
                    Terminated a -> a
                    InfiniteLoopDetected _ -> tryNext
         in case currentInstruction of
              Acc _ -> tryNext
              Jmp n -> tryThis (Nop n)
              Nop n -> tryThis (Jmp n)

parseInstruction :: ReadP Instruction
parseInstruction = parseAcc +++ parseJmp +++ parseNop
  where
    parseNum = do
      _ <- Parser.optional $ Parser.char '+'
      mNum <- readMaybe <$> Parser.munch1 (not . Char.isSpace)
      maybe Parser.pfail pure mNum
    parseAcc = do
      _ <- Parser.string "acc "
      Acc <$> parseNum
    parseJmp = do
      _ <- Parser.string "jmp "
      Jmp <$> parseNum
    parseNop = do
      _ <- Parser.string "nop "
      Nop <$> parseNum

parseInput :: ReadP [Instruction]
parseInput = Parser.sepBy1 parseInstruction (Parser.char '\n')

readInput :: IO [Instruction]
readInput = do
  str <- getContents
  case Parser.readP_to_S (parseInput <* Parser.eof) str of
    [(instructions, "")] -> pure instructions
    _ -> error "Invalid Input"
