{-# LANGUAGE FlexibleInstances #-}

module Day17 where

import Data.HashMap.Strict (HashMap)
import qualified Data.HashMap.Strict as HashMap
import Data.HashSet (HashSet)
import qualified Data.HashSet as HashSet
import Data.Hashable (Hashable)
import qualified Data.List as List
import Data.Maybe (catMaybes)

day17_1 :: IO ()
day17_1 = do
  initialState <- readInput
  let threeDState = HashSet.map (\(x, y) -> (x, y, 0 :: Int)) initialState
      after6Cycles = nTimes 6 runCycle threeDState
  putStrLn $ "Solution: " <> show (HashSet.size after6Cycles)

day17_2 :: IO ()
day17_2 = do
  initialState <- readInput
  let fourDState = HashSet.map (\(x, y) -> (x, y, 0 :: Int, 0 :: Int)) initialState
      after6Cycles = nTimes 6 runCycle fourDState
  putStrLn $ "Solution: " <> show (HashSet.size after6Cycles)

type Coords = (Int, Int, Int)

type HyperCoords = (Int, Int, Int, Int)

type ActiveCubes c = HashSet c

-- We go through active cubes and compute the "active neighbour count" of the
-- active cubes, this is called colouring for lack of a better word
type ColouredCubes c = HashMap c NeighbourCount

type NeighbourCount = Int

runCycle :: (Eq c, Hashable c, HasNeighbourCoords c) => ActiveCubes c -> ActiveCubes c
runCycle activeCubes =
  let neighbourCounts = HashSet.foldr updateNeighborsOf mempty activeCubes
      nextActives = HashMap.keysSet $ HashMap.filterWithKey (isNextStateActive activeCubes) neighbourCounts
   in nextActives

isNextStateActive :: (Eq c, Hashable c) => ActiveCubes c -> c -> NeighbourCount -> Bool
isNextStateActive activeCubes coords neighbours
  | HashSet.member coords activeCubes = neighbours == 2 || neighbours == 3
  | otherwise = neighbours == 3

updateNeighborsOf :: (Eq c, Hashable c, HasNeighbourCoords c) => c -> ColouredCubes c -> ColouredCubes c
updateNeighborsOf activeCoords cubes = foldr (\c -> HashMap.insertWith (+) c 1) cubes $ neighbourCoords activeCoords

class HasNeighbourCoords a where
  neighbourCoords :: a -> [a]

instance HasNeighbourCoords Coords where
  neighbourCoords (x, y, z) =
    List.delete
      (x, y, z)
      [ (x', y', z')
        | x' <- neighbours1D x,
          y' <- neighbours1D y,
          z' <- neighbours1D z
      ]

instance HasNeighbourCoords HyperCoords where
  neighbourCoords (x, y, z, w) =
    List.delete
      (x, y, z, w)
      [ (x', y', z', w')
        | x' <- neighbours1D x,
          y' <- neighbours1D y,
          z' <- neighbours1D z,
          w' <- neighbours1D w
      ]

neighbours1D :: Num a => a -> [a]
neighbours1D a = [a - 1, a, a + 1]

nTimes :: Int -> (a -> a) -> (a -> a)
nTimes 0 _ = id
nTimes 1 f = f
nTimes n f = f . nTimes (n -1) f

readInput :: IO (ActiveCubes (Int, Int))
readInput = do
  parseInput <$> getContents

parseInput :: String -> ActiveCubes (Int, Int)
parseInput s =
  let strs = lines s
   in HashSet.unions $
        zipWith parseLine strs [0 ..]
  where
    parseLine :: String -> Int -> HashSet (Int, Int)
    parseLine str y = HashSet.fromList $ catMaybes $ zipWith (parseChar y) [0 ..] str

    parseChar :: Int -> Int -> Char -> Maybe (Int, Int)
    parseChar y x c
      | c == '.' = Nothing
      | c == '#' = Just (x, y)
      | otherwise = error "Invalid input"
